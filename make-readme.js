#!/usr/bin/env node
/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
'use strict'

const fs = require('fs')
const { code2doc } = require('marcco')
const { rewriteRequires } = require('public-requires')
const { Snippin } = require('snippin')
const ST = require('stream-template')

// presentation helpers
const rr = src => rewriteRequires('@wraugh/cronlib', __dirname, __dirname, src)
const js2doc = src => code2doc(rr(src), { codePrefix: '~~~javascript' })

const s = new Snippin()
const pin = (file, snippet) => (s.getSnippetsSync(file))[snippet]
const pinDoc = (file, snippet) => js2doc(pin(file, snippet))

process.chdir(__dirname)
const out = fs.createWriteStream('./README.md')

ST`${pinDoc('test.js', 'readme')}

Contributing
------------

You're welcome to contribute to this project. If you make a Pull Request that

 - explains and solves a problem,
 - follows [standard style](https://standardjs.com/),
 - maintains 100% test coverage, and
 - keeps the documentation in sync with actual behaviour,

it _will_ be merged: this project follows the
[C4 process](https://rfc.zeromq.org/spec:42/C4/).

To make sure your commits follow the style guide and pass all tests, you can add

    ./.pre-commit

to your git pre-commit hook.
`.pipe(out)
